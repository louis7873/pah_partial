//just for testing
const followUpRecord = [{
    date: "2021/4/1",
    remark: "Some remarks 1",
    bookedTime: "12:00",
    alertTimeInMinute: 60
}, {
    date: "2021/4/2",
    bookedTime: "12:00",
    alertTimeInMinute: 160
}, {
    date: "2021/4/3",
    remark: "Some remarks 2",
    bookedTime: "12:00",
    alertTimeInMinute: 30
}, {
    date: "2021/4/4",
    bookedTime: "12:00",
    alertTimeInMinute: 60
}, {
    date: "2021/4/6",
    remark: "Some remarks 3",
    bookedTime: "12:00",
    alertTimeInMinute: 60
}]

export { followUpRecord }