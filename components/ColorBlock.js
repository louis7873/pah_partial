import * as React from 'react';
import { Text, View, TouchableHighlight, TouchableOpacity, ScrollView } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import host from '../constraint/host';
import postData from '../Util/post';
import Tooltip from 'rn-tooltip';

const USERCODE = 1;
const symptomsCountDetailWeekPHPLink = host + "/pah/AppData/symptomCountDetailWeek.php"
const symptomsCountDetailMonthPHPLink = host + "/pah/AppData/symptomCountDetailMonth.php"

const ColorBlock = (props) => {
    const [toolTipVisible, setToolTipVisible] = React.useState(false);
    const data = props.data.filter(record => record.id == props.category);
    return <View style={{ flex: props.flexAmount, backgroundColor: "#fff" }}>
        <Tooltip
            height={data.length * 40}
            width={200}
            containerStyle={{ borderWidth: 1 }}
            backgroundColor={"white"}
            pointerColor={"black"}
            popover={
                <View>
                    {data.length == 0 ? <Text>Loading...</Text> : data.map((item, index) => {
                        return <View style={{ flexDirection: "row", flex: 1, justifyContent: "space-between" }}>
                            <Text>{item.s_title_zh + "  "}</Text>
                            <Text>{item.code_count}</Text>
                        </View>
                    })}
                </View>
            }
        >
            <View style={{ height: 50, width: "100%", flexDirection: "column", height: 50 }}>
                <View style={{ backgroundColor: props.color, height: 23, marginTop: 7 }}></View>
                <Text style={{ alignSelf: "center", paddingBottom: 5 }}>{props.flexAmount}</Text>
            </View>
        </Tooltip>
    </View>;
}

export default ColorBlock;