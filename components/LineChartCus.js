import * as React from 'react';
import { LineChart } from 'react-native-chart-kit';
import { Text, View, StyleSheet, TouchableOpacity, Dimensions, ScrollView } from 'react-native';

const LineChartCus = (props) => {
    return <View style={{
        flexDirection: "row",
    }}><View style={{
        paddingHorizontal: 5
    }}>
            {[100, 75, 50, 25, 0].map(num => <Text style={{
                paddingVertical: 12, textAlign: "center"
            }}>{num}</Text>)}
            <Text style={{
                textAlign: "center",
                fontSize:10
            }}>{props.yAxisLabel+"/date"}</Text>
        </View><ScrollView
            style={{
                marginVertical: 8,
                borderRadius: 16,
            }}
            horizontal={true}
            contentOffset={{ x: 0, y: 0 }} // i needed the scrolling to start from the end not the start
            showsHorizontalScrollIndicator={false} // to hide scroll bar
        >
            <LineChart
                data={{
                    labels: props.labels,
                    datasets: props.data.concat({
                        data: Array(12).fill(100),
                        color: () => `rgba(0, 0, 0, 0)`
                    })
                }}
                withVerticalLines={false}
                withHorizontalLabels={false}
                width={props.width?props.width:350} // from react-native
                height={220}
                yAxisSuffix={props.yAxisLabel}
                style={{
                    paddingRight: 10,
                }}
                chartConfig={{
                    propsForDots: {
                        r: "3",
                        strokeWidth: "2",
                    },
                    strokeWidth: 2,
                    backgroundGradientFrom: '#fff',
                    backgroundGradientTo: '#fff',
                    decimalPlaces: 0, // optional, defaults to 2dp
                    color: (opacity = 0) => ``,
                    style: {
                        borderRadius: 16,
                    },
                    propsForVerticalLabels: {
                        fill: "#000"
                    },
                    propsForHorizontalLabels: {
                        fill: "#000",
                    },
                    propsForBackgroundLines: {
                        stroke: "#ddd",
                    },
                    propsForLabels: {
                        fontSize: 10
                    },
                }}
                fromZero
                verticalLabelRotation={315}
            /></ScrollView></View>;
}

export default LineChartCus;