const language = "zh_hk";

const i18n = {
    "zh_hk": {
        "lang": "zh_hk",
        //navigation
        "Home_title": "PAH 健康管家",
        "FollowUpDate_title": "覆診日期",
        "FollowUpRecord_title": "覆診記錄",
        "Today_Home_title": "症狀記錄",
        "Main_Home_title": "主頁",
        "Main_Drug_title": "藥物紀錄",
        "Main_Dairy_title": "今日症狀",
        "Main_Chart_title": "過往記錄",
        "Main_Info_title": "疾病資訊",
        //Home
        "Home_buttonText": "登記下一次覆診日期",
        "Home_nextDay": "下一次覆診日期",
        "Home_textInGap": "今日",
        "Home_buttonTextCheck": "症狀檢查",
        "Home_greetings": "你覺得今天身體狀況如何？",
        "Home_tipsTitle": "肺動脈高壓 生活小貼士",
        "Home_tipsText": "改善日常生活習慣有助舒緩症狀，\n提升生活質素",
        "Home_btnMoreText": "了解更多",
        //FollowUpRecord
        "FollowUpRecord_followUpTitleDate": "覆診日期",
        "FollowUpRecord_followUpTitleRemark": "備忘錄",
        "FollowUpRecord_btnNewDateRecord": "新增下次覆診日期",
        "FollowUpRecord_btnCreateText": "覆診備忘錄",
        "FollowUpRecord_ConfirmText": "確定",
        "FollowUpRecord_Delete": "Delete",
        "FollowUpRecord_Placeholder": "覆診備忘錄內容(最多100個字符)",
        //FollowUpDate
        "FollowUpDate_hintDateTitle": "覆診日期",
        "FollowUpDate_bookTime": "預約時間",
        "FollowUpDate_bookAlarm": "設定覆診通知",
        "FollowUpDate_choosing": "...選擇",
        "FollowUpDate_CreateUpdate": "新増・更新",
        //global
        "dayFunc": (str) => {
            let date = new Date(str);
            return date.getFullYear() + "年"
                + (date.getMonth() + 1) + "月"
                + (date.getDate() ) + "日";
        },
        "dayFuncT": (str) => {
            let date = new Date(str);
            return date.toDateString().substring(4);
        },
        "dayFuncWithOutYear": (str) => {
            let date = new Date(str);
            return (date.getMonth() + 1) + "月"
                + (date.getDate()) + "日";
        },
    },
    "en_us": {
        "lang": "en_us",
        "Home_title": "",
        "dayFunc": (str) => {
            let date = new Date(str);
            return date.toDateString().substring(4);
        },
        "dayFuncWithOutYear": (str) => {
            let date = new Date(str);
            return date.toDateString().substring(4, 10);
        }
    }
}

export default i18n[language];