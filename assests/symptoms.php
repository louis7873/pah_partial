<?php 

include("Connection/db.php");

function getAllSymptoms(){
  global $con; 
  $sql = "SELECT * FROM symptom_list";
  $stmt = $con->prepare($sql);
  $stmt->execute(); 
  while ($row = $stmt->fetch()) {
    $obj['s_title_zh'] = $row['s_title_zh'];
    $obj['s_title_en'] = $row['s_title_en'];
    $obj['s_category'] = $row['s_category'];
    $obj['s_icon'] = $row['s_icon'];
    $obj['s_icon_active'] = $row['s_icon_active'];
    $symptoms_arr[] = $obj;
  }

  return $symptoms_arr;

}

// function getSymptom(){
//   global $con; 


// }


echo json_encode(getAllSymptoms(), JSON_UNESCAPED_UNICODE);




?>