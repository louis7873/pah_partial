/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */


import * as React from 'react';
import { View, Text, Button, TouchableOpacity, Image } from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './screens/Home';
import FollowUpDate from './screens/FollowUpDate';
import FollowUpRecord from './screens/FollowUpRecord';
import TodaySymptoms from './screens/TodaySymptoms';
import i18n from "./constraint/i18n";
import OptionSelect from './screens/OptionSelect';
import Login from './screens/Login';
import Register from './screens/Register';
import PastRecord from "./screens/PastRecord";
import TodaySymptoms3 from './screens/TodaySymptoms3';

const translator = i18n;

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();

let iconPack = {
  "Main_Home": require("./assests/PAH_img/Main_Home.png"),
  "Main_Drug": require("./assests/PAH_img/Main_Drug.png"),
  "Main_Dairy": require("./assests/PAH_img/Main_Dairy.png"),
  "Main_Chart": require("./assests/PAH_img/Main_Chart.png"),
  "Main_Info": require("./assests/PAH_img/Main_Info.png"),

  "Main_Home_a": require("./assests/PAH_img/Main_Home_a.png"),
  "Main_Drug_a": require("./assests/PAH_img/Main_Drug_a.png"),
  "Main_Dairy_a": require("./assests/PAH_img/Main_Dairy_a.png"),
  "Main_Chart_a": require("./assests/PAH_img/Main_Chart_a.png"),
  "Main_Info_a": require("./assests/PAH_img/Main_Info_a.png"),
}

function Main() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home"
        component={Home}
        options={({ navigation }) => ({
          title: translator.Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , headerLeft: () => (
            <TouchableOpacity onPress={() => {
              navigation.navigate("Option")
            }} >
              <View style={{ backgroundColor: "#ddd", width: 35, height: 35, margin: 10 }}></View>
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen name="FollowUpDate"
        component={FollowUpDate}
        options={{
          title: translator.FollowUpDate_title
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , ...styles.header
        }}
      />
      <Stack.Screen name="FollowUpRecord"
        component={FollowUpRecord}
        options={{
          title: translator.FollowUpRecord_title
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , ...styles.header
        }}
      />
    </Stack.Navigator>
  );
}

function PastR() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="PastRecord"
        component={PastRecord}
        options={({ navigation }) => ({
          title: "過往症狀紀錄"
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
        })}
      />
    </Stack.Navigator>
  );
}

function Today() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Today_Home"
        component={TodaySymptoms3}
        options={({ navigation }) => ({
          title: translator.Today_Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , headerLeft: () => (
            <TouchableOpacity>
              <View style={{ backgroundColor: "#ddd", width: 35, height: 35, margin: 10 }}></View>
            </TouchableOpacity>
          )
          , headerRight: () => (
            <TouchableOpacity onPress={() => {
              navigation.navigate("Today_Home", { showCal: true })
            }}>
              <View style={{ backgroundColor: "#ddd", width: 35, height: 35, margin: 10 }}></View>
            </TouchableOpacity>
          )
        })}
      />
    </Stack.Navigator>
  );
}

function TodayNext() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Today_Home"
        component={TodaySymptoms3}
        options={{
          title: translator.Today_Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , headerLeft: () => (
            <TouchableOpacity>
              <View style={{ backgroundColor: "#ddd", width: 35, height: 35, margin: 10 }}></View>
            </TouchableOpacity>
          ),
        }}
      />
    </Stack.Navigator>
  );
}

function Option() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="OptionSelect"
        component={OptionSelect}
        options={({ navigation }) => ({
          title: translator.Today_Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
          , headerLeft: () => (
            <TouchableOpacity onPress={() => {
              navigation.navigate("Home")
            }} >
              <View style={{ backgroundColor: "#ddd", width: 35, height: 35, margin: 10 }}></View>
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen name="Login"
        component={Login}
        options={({ navigation }) => ({
          title: translator.Today_Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
        })}
      />
      <Stack.Screen name="Register"
        component={Register}
        options={({ navigation }) => ({
          title: translator.Today_Home_title
          , ...styles.header
          , headerBackTitle: ""
          , headerTruncatedBackTitle: ""
        })}
      />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer theme={defaultScreen}>
      <Tab.Navigator
        screenOptions={({ route }) => {
          return ({
            tabBarIcon: ({ focused, color, size }) => {
              console.log(route.name);
              return (
                <View style={{ width: 30, height: 30, backgroundColor: "rgba(0,0,0,0)", marginTop: 0 }}>
                  {color == 'gray' ?
                    <Image style={{ width: "100%" }} resizeMode="contain" source={iconPack[route.name]} /> :
                    <Image style={{ width: "100%" }} resizeMode="contain" source={iconPack[route.name + "_a"]} />}
                </View>
              );
            },
            tabBarButton: [
              "Option",
              "Login",
              "Register"
            ].includes(route.name)
              ? () => {
                return null;
              }
              : undefined,
          })
        }}
        tabBarOptions={{
          activeTintColor: '#357d8a',
          inactiveTintColor: 'gray',
          style: { height: 90 }
        }}
      >
        <Tab.Screen name="Main_Home" component={Main} options={{ title: translator.Main_Home_title }} />
        <Tab.Screen name="Main_Drug" component={TodayNext} options={{ title: translator.Main_Drug_title }} />
        <Tab.Screen name="Main_Dairy" component={Today} options={{ title: translator.Main_Dairy_title }} />
        <Tab.Screen name="Main_Chart" component={PastR} options={{ title: translator.Main_Chart_title }} />
        <Tab.Screen name="Main_Info" component={Main} options={{ title: translator.Main_Info_title }} />
        <Tab.Screen name="Option" component={Option} options={{ tabBarVisible: false }} labeled={false} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const defaultScreen = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white'
  }
}

const styles = {
  header: {
    headerStyle: {
      height: 110,
      backgroundColor: "#acddd4"
    },
    headerTintColor: "#086b7a",
    headerTitleStyle: {
      fontWeight: 'bold',
      paddingBottom: 0,
      fontSize: 25
    }
  }
};

export default App;
