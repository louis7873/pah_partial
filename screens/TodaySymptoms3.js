import * as React from 'react';
import {
    View, Text, StyleSheet, FlatList,
    Image, TouchableOpacity, Modal,
    TouchableHighlight, TextInput
} from 'react-native';
import Colors from '../constraint/Colors';
import host from '../constraint/host';
import imgForSymptoms from '../constraint/imgForSymptoms';
import postData from '../Util/post';
import i18n from "../constraint/i18n";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const translator = i18n;
const symptomsListPHPLink = host + "/pah/AppData/symptoms.php";
const symptomsCUDPHPLink = host + "/pah/AppData/symptomsCUD.php";//for create/update/delete
//const symptomsRPHPLink = host + "/pah/AppData/symptomsR.php";//for read only
const symptomsNotePHPLink = host + "/pah/AppData/symptomNote.php";//for create/update/delete
const imgLink = host + "/pah/";
const USERCODE = "1";

export default function TodaySymptoms3(props) {
    const flatlistRef = React.useRef();
    const [record, setRecord] = React.useState([]);
    const [currentDate, setCurrentDate] = React.useState(new Date);
    const [modalVisible, setModalVisible] = React.useState(false);
    const [remarkDetail, setRemarkDetail] = React.useState({});
    const [topMoreVisible, setTopMoreVisible] = React.useState(false);
    const [bottomMoreVisible, setBottomMoreVisible] = React.useState(true);
    const [otherRemarks, setOtherRemarks] = React.useState("");
    const [data, setData] = React.useState([]);
    const [showDatePicker, setShowDatePicker] = React.useState(false);
    React.useEffect(() => {
        let date = new Date();
        //time zone will occur bug, consider to add this code in i18n.js
        date.setHours(date.getHours() + 8);
        //above indicating the time offset in hong kong
        refreshSelectionByDay(date);
    }, []);
    React.useLayoutEffect(() => {
        if (props.route.params) {
            if (props.route.params.showCal) {
                setDatePickerVisibile(true);
            }
        }
    }, [props.route.params]);
    const [datePickerVisible, setDatePickerVisibile] = React.useState(false);
    const confirmDate = (date) => {
        refreshSelectionByDay(date);
    };
    const groupData = (data) => {
        // const addBannerIndex = data.length-3;
        let dataMaxCat = data[data.length - 1].s_category;
        const result = [];
        for (let i = 0; i < dataMaxCat + 1; i++) {
            let currentData = data.filter(item => item.s_category == i);
            if (currentData.length != 0) {
                result.push(currentData);
            }
        }
        return result;
    }
    const refreshSelectionByDay = (date_, num = 0) => {
        let date = new Date(date_);
        date.setDate(date.getDate() + num);
        setCurrentDate(date);
        postData(symptomsListPHPLink, {
            date: date.toISOString().substring(0, 10),
            user_code: USERCODE
        }).then(json => {
            if (json != null) {
                let modified = groupData(json.filter(item => item.mode != 'Note'));
                setRecord(modified);
                setOtherRemarks("");
                let tempData = json.map(item => {
                    let type = null;
                    let value = null;
                    if (item.mode == "boolean") {
                        type = "boolean";
                        value = false;
                    } else if (item.mode == "text") {
                        type = "text";
                        value = "";
                    }
                    if (json != null) {
                        if (item.date != null) {
                            if (item.mode == "boolean") {
                                value = true;
                            } else if (item.mode == "text") {
                                value = item.remarks;
                            }
                        } else if (item.mode == "Note") {
                            console.log(item.remarks)
                            setOtherRemarks(item.remarks);
                            return null;
                        }
                    }
                    return { code: item.code, type: type, value: value };
                });
                setData(tempData.filter(item => item != null));
            }
        }).catch(err => { throw err; });
    }
    const minusOneDay = () => {
        refreshSelectionByDay(currentDate, -1);
    }
    const plusOneDay = () => {
        refreshSelectionByDay(currentDate, 1);
    }
    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };
    const showModal = (remark) => {
        setRemarkDetail(remark);
        setModalVisible(true);
    }
    const updateSelected = (code) => {
        let tempRecord = [...data];
        tempRecord = tempRecord.map(item => {
            if (item.code == code) {
                item.value = !item.value;
            }
            return item;
        })
        setData(tempRecord);
    }
    const updateNote = (code, text) => {
        let tempRecord = [...data];
        tempRecord = tempRecord.map(item => {
            if (item.code == code) {
                item.value = text;
            }
            return item;
        })
        setData(tempRecord);
    }
    const saveRecord = async () => {
        let err_note = await postData(symptomsNotePHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            scode: [].concat(...record)
                .filter(item => item.mode == "text")
                .map(item => data.find(dataitem => dataitem.code == item.code))
                .filter(item => item.value != "" && item.value != null)
                .map(item =>
                    item.code),
            svalue: [].concat(...record)
                .filter(item => item.mode == "text")
                .map(item => data.find(dataitem => dataitem.code == item.code))
                .filter(item => item.value != "" && item.value != null)
                .map(item =>
                    item.value)
        }).then(() => null, err => err);
        if (err_note == null) {
            console.log("success: " + symptomsNotePHPLink);
        }
        let err_symptom = await postData(symptomsCUDPHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            note: otherRemarks,
            scodes: [].concat(...record)
                .filter(item => item.mode == "boolean")
                .map(item =>
                    ({ code: item.code, value: item.value }))
                .map(item => data.find(dataitem => dataitem.code == item.code))
                .filter(item => item.value)
                .map(item => item.code)
        }).then(() => null, err => err);
        if (err_symptom == null) {
            console.log("success: " + symptomsCUDPHPLink);
        }
    }
    return <View style={{
        flex: 1,
        alignItems: "center",
    }}>
        <DateTimePickerModal
            isVisible={datePickerVisible}
            mode="date"
            onConfirm={(date) => {
                confirmDate(date);
                setDatePickerVisibile(false);
            }}
            onCancel={() => { setDatePickerVisibile(false) }}
        />
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(!modalVisible);
            }}
        >
            <TouchableHighlight onPress={() => {
                setModalVisible(!modalVisible)
            }}>
                <View style={{ backgroundColor: "rgba(0, 0, 0, 0.5)", width: "100%", height: "100%" }}>
                    <View style={{ backgroundColor: "white", marginVertical: "40%", marginHorizontal: 5, height: "60%", borderRadius: 10, padding: 10 }}>
                        <View style={{ flexDirection: "row", width: "100%", justifyContent: "space-between" }}>
                            <View style={{ padding: 5, flexDirection: "row" }}>
                                <TouchableOpacity onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                    <Text>✖</Text>
                                </TouchableOpacity>
                                <Text>Delete</Text>
                            </View>
                            <TouchableOpacity
                                style={{ alignSelf: "flex-end", borderColor: Colors.gray, borderRadius: 5, borderWidth: 1, padding: 5 }}
                                onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                <Text>確定</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}>
                            <Text style={{ fontSize: 25, padding: 5 }}>
                                {remarkDetail.title}
                            </Text>
                        </View>
                        {remarkDetail.title === "特別事項記錄" ?
                            <>
                                <TextInput
                                    style={{ margin: 5, borderWidth: 1, height: 200 }}
                                    multiline={true}
                                    numberOfLines={4}
                                    value={otherRemarks}
                                    onChangeText={setOtherRemarks}
                                    placeholder={"請輸入你要記錄的內容（最多100個字符）"} />
                            </>
                            :
                            <View style={{ flexDirection: "row" }}>
                                <TextInput
                                    onChangeText={(text) => { remarkDetail == null ? () => { } : updateNote(remarkDetail.code, text.replace(/[^0-9]/g, '')) }}
                                    value={data.find(item => item.code == remarkDetail.code) != null ? data.find(item => item.code == remarkDetail.code).value : ""}
                                    style={{ margin: 5, borderWidth: 1, borderColor: "#ddd", height: 30, fontSize: 20, width: "75%" }} />
                                <Text style={{ margin: 6, height: 30, fontSize: 20 }}>
                                    {
                                        remarkDetail.suffix
                                    }
                                </Text>
                            </View>
                        }
                    </View>
                </View>
            </TouchableHighlight>
        </Modal>
        <View style={styles.dateView}>
            <TouchableOpacity onPress={minusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    prev
                </Text>
            </TouchableOpacity>
            <Text style={styles.dateText}>
                {translator.dayFunc(currentDate)}
            </Text>
            <TouchableOpacity onPress={plusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    next
                </Text>
            </TouchableOpacity>
        </View>
        <FlatList data={record}
            ref={flatlistRef}
            onScroll={({ nativeEvent }) => {
                setBottomMoreVisible(true);
                setTopMoreVisible(true);
                if (isCloseToBottom(nativeEvent)) {
                    setBottomMoreVisible(false);
                }
                if (nativeEvent.contentOffset.y <= 0) {
                    console.log("start");
                    setTopMoreVisible(false);
                }
            }}
            scrollEventThrottle={400}
            ListHeaderComponent={() => {
                return (
                    <View style={{ flexDirection: "row", padding: 7 }}>
                        <Text style={{ width: "50%", textAlign: "center" }}>症狀ON</Text>
                        <Text style={{ width: "50%", textAlign: "center" }}>症狀OFF</Text>
                    </View>
                );
            }}
            renderItem={({ item, index }) => {
                return <FlatList numColumns={2} data={item} renderItem={(subitem) => {
                    let info = data.find(item => item.code == subitem.item.code);
                    return <TouchableOpacity
                        style={{ width: "50%", paddingVertical: 10, justifyContent: 'center' }}
                        onPress={() => {
                            if (subitem.item.mode != "text") {
                                updateSelected(subitem.item.code);
                            } else {
                                showModal({
                                    title: subitem.item.s_title_zh,
                                    code: subitem.item.code,
                                    suffix: subitem.item.suffix
                                });
                            }
                        }}
                        style={{ flex: 1 / 2 }}
                    >
                        <View>
                            <Text>{JSON.stringify(info)}</Text>
                            {subitem.item.mode == "text" ?
                                <>
                                    <Image source={{ uri: imgLink + subitem.item.s_icon }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                    <Text style={{ alignSelf: "center", fontSize: 18, paddingVertical: 6 }}>{subitem.item.s_title_zh}</Text>
                                </> :
                                (subitem.item.mode == "boolean" && info != null &&
                                    <>
                                        {
                                            info.value == true ?
                                                <Image source={{ uri: imgLink + subitem.item.s_icon_active }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                                :
                                                <Image source={{ uri: imgLink + subitem.item.s_icon }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                        }
                                        <Text style={{ alignSelf: "center", fontSize: 18, paddingVertical: 6 }}>{subitem.item.s_title_zh}</Text>
                                    </>)
                            }
                        </View>
                    </TouchableOpacity>
                }
                } />
            }}
            ListFooterComponent={() => {
                return (
                    <View style={{ width: "100%", alignItems: "center", paddingBottom: 20 }}>
                        <TouchableOpacity style={{ width: "80%" }} onPress={() => {
                            showModal({ title: "特別事項記錄" });
                        }}>
                            <View style={{ backgroundColor: "#eeeded", alignItems: "center", paddingVertical: 8, borderRadius: 100 }}>
                                <Text style={{ color: "#807bb4", fontSize: 20 }}>其他注意事項</Text>
                            </View>
                        </TouchableOpacity>
                    </View>);
            }} />
        < View style={{ position: 'absolute', bottom: 80, display: (!bottomMoreVisible || !topMoreVisible) ? "flex" : "none" }}>
            <TouchableOpacity onPress={() => {
                if (topMoreVisible) {
                    flatlistRef.current.scrollToOffset({ offset: 0, animated: true })
                } else {
                    flatlistRef.current.scrollToEnd({ animating: true });
                }
            }}>
                <Text>{topMoreVisible ? "Up" : "Down"}</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.saveArea}>
            <TouchableOpacity style={styles.btnMore} onPress={saveRecord}>
                <Text style={styles.btnMoreText}>儲存</Text>
            </TouchableOpacity>
        </View>
    </View >
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "white",
    },
    dateView: {
        flexDirection: "row",
        backgroundColor: "#ddd",
        width: "100%",
        justifyContent: "space-between",
        padding: 7
    },
    dateText: {
        fontSize: 22,
    },
    btnMore: {
        backgroundColor: "#b497c6",
        width: "90%",
        padding: 10,
        marginVertical: 3,
        marginHorizontal: "5%",
        borderRadius: 20,
        alignItems: "center",
    },
    btnMoreText: {
        color: "white",
        fontWeight: "bold",
        fontSize: 23
    },
    saveArea: {
        alignItems: "center",
        padding: 15,
        backgroundColor: "#ddd",
        height: "12%",
        width: "100%"
    }
})