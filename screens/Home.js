import * as React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '../constraint/Colors';
import { medicationTime } from '../Data/TestingMedicationDra';
import { useState, useEffect } from 'react';
import { FlatList } from 'react-native-gesture-handler';
import i18n from "../constraint/i18n";

const translator = i18n;

export default function Home(props) {
  const [followUpDateText, setFollowUpDateText] = React.useState("null");
  const [medicationRecord, setMedicationRecord] = React.useState([]);
  useEffect(() => {
    setFollowUpDateText(i18n.dayFunc("2021/4/1"));
    setMedicationRecord(medicationTime);
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.section}>
        <TouchableOpacity style={styles.button} onPress={() => {
          props.navigation.navigate("FollowUpRecord")
        }}>
          <Text style={styles.buttonText}>{translator.Home_buttonText}</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row" }}>
          <View style={styles.imgReal}>
            <Image resizeMode="contain" style={{ width: "100%", flex: 1 }} source={require("../assests/PAH_img/hospital_copy.png")} />
          </View>
          <View style={styles.hintDateArea}>
            <Text style={{ color: Colors.gray }}>{translator.Home_nextDay}</Text>
            <Text style={styles.hintDate}>{followUpDateText}</Text>
          </View>
        </View>
      </View>
      <Text style={styles.textInGap}>{translator.Home_textInGap}</Text>
      <View style={{ ...styles.section, margin: 5 }}>
        <FlatList
          style={{ width: "100%", height: 140 }}
          data={medicationRecord}
          renderItem={({ item, index }) => {
            return (
              <View style={styles.medicationRecord}>
                <View style={{ flex: 3, alignItems: "flex-end", marginRight: 25 }}>
                  <Text style={{ color: "#58b1ce", fontWeight: "bold", fontSize: 13 }}>{item.time}</Text>
                  <Text style={{ fontSize: 11, color: "#757575" }}>{item.peroid}</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <View style={{ ...styles.img, borderRadius: 17 }}></View>
                </View>
                <View style={{ flex: 9 }}>
                  <Text style={{ color: "#626262", fontWeight: "bold", fontSize: 15 }}>{item.medicineName}</Text>
                  <Text style={{ color: "#656565" }}>{item.amountUnit}</Text>
                </View>
                <View style={{ flex: 3 }}>
                  <View style={styles.img}></View>
                </View>
              </View>
            );
          }}
        />
      </View>
      <View style={{ ...styles.section, flexDirection: "row" }}>
        <View style={{ ...styles.imgReal, height: "100%" }}>
          <Image resizeMode="contain" style={{ width: "100%", flex: 1 }} source={require("../assests/PAH_img/health-check.png")} /></View>
        <View style={{ flex: 9 }}>
          <TouchableOpacity style={{ marginLeft: 10 }}>
            <Text style={{ ...styles.buttonText, fontSize: 26 }}>{translator.Home_buttonTextCheck}</Text>
          </TouchableOpacity>
          <Text style={{ margin: 10, color: Colors.gray }}>{translator.Home_greetings}</Text>
        </View>
      </View>
      <View style={{ width: "100%" }}>
        <ImageBackground
          resizeMode="contain"
          source={require("../assests/PAH_img/lifestyle-01.png")}
          style={{
            height: 220,
          }}>
          <Text style={styles.tipsTitle}>
            {translator.Home_tipsTitle}
          </Text>
          <Text style={styles.tipsText}>
            {translator.Home_tipsText}
          </Text>
          <TouchableOpacity style={styles.btnMore}>
            <Text style={styles.btnMoreText}>{translator.Home_btnMoreText}</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    </View >
  );
}

const styles = StyleSheet.create({
  btnMore: {
    backgroundColor: "#b497c6",
    width: 80,
    padding: 5,
    marginVertical: 3,
    marginHorizontal: "5%",
    borderRadius: 7,
    alignItems: "center",
  },
  btnMoreText: {
    color: "white"
  },
  tipsText: {
    alignSelf: 'flex-start',
    marginHorizontal: "5%",
    color: Colors.gray
  },
  tipsTitle: {
    alignSelf: 'flex-start',
    fontSize: 24,
    marginHorizontal: "5%",
    marginVertical: 3,
    color: "#b497c6",
    fontWeight: "bold"
  },
  medicationRecord: {
    flexDirection: "row",
    justifyContent: "center",
    width: "100%",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
    padding: 7
  },
  textInGap: {
    alignSelf: 'flex-start',
    marginHorizontal: "5%",
    color: Colors.gray
  },
  hintDate: {
    color: "#555",
    fontSize: 30
  },
  hintDateArea: {
    flex: 7,
    marginHorizontal: 7
  },
  img: {
    flex: 3,
    backgroundColor: "gray",
    marginHorizontal: 7
  },
  imgReal: {
    flex: 3,
    backgroundColor: "rgba(0,0,0,0)",
    marginHorizontal: 7
  },
  container: {
    alignItems: "center",
    paddingTop: 15,
    backgroundColor: "white",
  },
  section: {
    backgroundColor: "#f8f8f8",
    borderRadius: 5,
    width: "90%",
    padding: 10,
    marginBottom: 15,
    shadowColor: "#ddd",
    shadowOpacity: 1,
    shadowOffset: {
      width: 3,
      height: 3
    },
    alignItems: "center",
  },
  button: {
    alignItems: "center",
    paddingBottom: 15,
    paddingTop: 5
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: "bold",
    fontSize: 20
  },

});