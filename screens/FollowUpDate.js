import * as React from "react";

import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native';
import Colors from "../constraint/Colors";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import SelectPicker from 'react-native-form-select-picker';
import i18n from "../constraint/i18n";

const translator = i18n;
export default function FollowUpDate(props) {
    const options = ["覆診時間半小時前", "覆診時間一小時前", "覆診時間兩小時前"]
    const [selected, setSelected] = React.useState();
    const [datePickerVisible, setDatePickerVisibile] = React.useState(false);
    const [timePickerVisible, setTimePickerVisibile] = React.useState(false);
    const [showDate, setShowDate] = React.useState("");
    const [showTime, setShowTime] = React.useState("設定時間");
    const getDate = (str) => {
        return new Date(str);
    }
    const confirmDate = (date) => {
        setShowDate(translator.dayFunc(date))
    };
    const confirmTime = (time) => {
        let confirmedTime = getDate(time);
        setShowTime(confirmedTime.getHours() % 12 + ":"
            + (confirmedTime.getMinutes() < 10 ? "0" + confirmedTime.getMinutes() : confirmedTime.getMinutes()) + " "
            + (confirmedTime.getHours() >= 12 ? "PM" : "AM"));
    };
    React.useEffect(() => {
        confirmDate(props.route.params.defaultDate)
    }, [])
    return (
        <View style={styles.container}>
            <DateTimePickerModal
                isVisible={datePickerVisible}
                mode="date"
                onConfirm={(date) => {
                    confirmDate(date);
                    setDatePickerVisibile(false);
                }}
                onCancel={() => { setDatePickerVisibile(false) }}
            />
            <DateTimePickerModal
                isVisible={timePickerVisible}
                mode="time"
                onConfirm={(time) => {
                    confirmTime(time);
                    setTimePickerVisibile(false);
                }}
                onCancel={() => { setTimePickerVisibile(false) }}
            />
            <View style={styles.section}>
                <View style={{ flexDirection: "row" }}>
                    <View style={{
                        ...styles.imgReal,
                        flex: 2
                    }}>
                        <Image resizeMode="contain" style={{ width: "100%", flex: 1 }} source={require("../assests/PAH_img/hospital_copy.png")} />
                    </View>
                    <View style={styles.hintDateArea}>
                        <Text style={styles.hintDateTitle}>{translator.FollowUpDate_hintDateTitle}</Text>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ ...styles.hintDate, flex: 10, fontWeight: "bold" }}>{showDate}</Text>
                            <TouchableOpacity style={{
                                ...styles.img,
                                flex: 2
                            }} onPress={() => {
                                setDatePickerVisibile(true)
                            }}>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.optionSection}>
                <Text style={{ fontSize: 16 }}>{translator.FollowUpDate_bookTime}</Text>
                <TouchableOpacity onPress={() => {
                    setTimePickerVisibile(true)
                }}>
                    <Text style={{ fontSize: 16 }}>{showTime}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.optionSection}>
                <Text style={{ fontSize: 16 }}>{translator.FollowUpDate_bookAlarm}</Text>
                <SelectPicker
                    onValueChange={(value) => {
                        // Do anything you want with the value. 
                        // For example, save in state.
                        setSelected(value);
                    }}
                    placeholder={translator.FollowUpDate_choosing}
                    style={{ padding: 0 }}
                    selected={selected}
                >
                    {Object.values(options).map((val, index) => (
                        <SelectPicker.Item label={val} value={val} key={index} />
                    ))}
                </SelectPicker>
            </View>
            <View style={styles.btnSection}>
                <TouchableOpacity style={styles.btn}>
                    <Text style={styles.btnText}>{translator.FollowUpDate_CreateUpdate}</Text>
                </TouchableOpacity>
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    btnSection: {
        justifyContent: "center",
        flexDirection: "row",
        paddingVertical: 15,
        borderTopWidth: 1,
        borderTopColor: "#ddd",
        width: "90%"
    },
    optionSection: {
        justifyContent: "space-between",
        flexDirection: "row",
        paddingVertical: 15,
        borderTopWidth: 1,
        borderTopColor: "#ddd",
        width: "90%"
    },
    btnText: {
        color: "white",
        fontWeight: "bold",
        fontSize: 20
    },
    btn: {
        backgroundColor: "#b497c6",
        padding: 8,
        borderRadius: 10,
        alignItems: "center",
        width: "80%",
        alignSelf: "center"
    },
    hintDateTitle: {
        color: "#58b1ce",
        fontWeight: "bold"
    },
    hintDate: {
        color: "#555",
        fontSize: 25
    },
    hintDateArea: {
        flex: 7,
        marginHorizontal: 7
    },
    img: {
        backgroundColor: "gray",
        marginHorizontal: 7
    },
    imgReal: {
        flex: 3,
        backgroundColor: "rgba(0,0,0,0)",
        marginHorizontal: 7
    },
    container: {
        alignItems: "center",
        paddingTop: 15,
        backgroundColor: "white",
    },
    section: {
        backgroundColor: "#f8f8f8",
        borderRadius: 5,
        width: "90%",
        padding: 10,
        marginBottom: 15,
        shadowColor: "#ddd",
        shadowOpacity: 1,
        shadowOffset: {
            width: 3,
            height: 3
        },
        alignItems: "center",
    }
})