import * as React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

export default function OptionSelect(props) {
    return (
        <View sytyle={{ flex: 1, width: "100%" }}>
            <TouchableOpacity onPress={()=>{
                props.navigation.navigate("Login")
            }}>
                <View style={styles.optionRow}>
                    <View style={styles.optionIcon}>

                    </View>
                    <View style={styles.optionTextContainer}>
                        <Text style={styles.optionText}>會員登記/登入</Text>
                    </View>
                </View>
            </TouchableOpacity>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>覆診紀錄一覽</Text>
                </View>
            </View>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>通知設定</Text>
                </View>
            </View>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>語言選擇</Text>
                </View>
            </View>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>一般條款及細則</Text>
                </View>
            </View>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>私隱政策聱明</Text>
                </View>
            </View>
            <View style={styles.optionRow}>
                <View style={styles.optionIcon}>

                </View>
                <View style={styles.optionTextContainer}>
                    <Text style={styles.optionText}>聯絡我們</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    optionRow: {
        flexDirection: "row",
        margin: 3
    },
    optionText: {
        fontSize: 20,
        color: "#999"
    },
    optionTextContainer: {
        paddingVertical: 11,
        paddingLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#ddd",
        width: "100%"
    },
    optionIcon: {
        width: 44,
        height: 44,
        backgroundColor: "#ddd"
    }
})