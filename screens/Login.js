import * as React from "react";
import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity } from "react-native";
import Colors from "../constraint/Colors";

export default function Login(props) {
    const [email, onChangeEmail] = React.useState('');
    const [password, onChangePassword] = React.useState('');
    return (<View style={{ backgroundColor: "#ddd", flex: 1 }}>
        <View style={{ padding: 7, marginTop: "15%" }}>
            <Text style={styles.textTitle}>電郵</Text>
            <View style={styles.sectionStyle}>
                <View
                    style={styles.imageStyle}
                />
                <TextInput
                    onChangeText={(text) => onChangeEmail(text)}
                    style={{ flex: 1 }}
                    placeholder="Enter Your Email Here"
                    underlineColorAndroid="transparent"
                />
            </View>
        </View>
        <View style={{ padding: 7 }}>
            <Text style={styles.textTitle}>密碼</Text>
            <View style={styles.sectionStyle}>
                <View
                    style={styles.imageStyle}
                />
                <TextInput
                    onChangeText={(text) => onChangePassword(text)}
                    style={{ flex: 1 }}
                    placeholder="Enter Your Password Here"
                    underlineColorAndroid="transparent"
                />
            </View>
        </View>
        <Text style={styles.foretPassword}>忘記密碼?</Text>
        <TouchableOpacity style={styles.btnLogin}>
            <Text style={styles.btnLoginText}>登入</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
            <Text style={styles.askText}>尚未有會員帳戶？</Text>
            <Text style={{ ...styles.askText, textDecorationLine: 'underline' }}>建立帳戶</Text>
        </View>
    </View>)
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
    },
    sectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderWidth: 0.5,
        width: "90%",
        alignSelf: "center",
        borderColor: '#000',
        height: 40,
        borderRadius: 18,
        marginVertical: 10,
    },
    imageStyle: {
        backgroundColor: "#ddd",
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
        borderRightWidth: 1,
        borderRightColor: "#ddd"
    },
    btnLogin: {
        backgroundColor: "#b497c6",
        width: "85%",
        alignSelf: "center",
        padding: 5,
        marginVertical: 3,
        marginHorizontal: "5%",
        borderRadius: 20,
        alignItems: "center",
    },
    btnLoginText: {
        color: "white",
        paddingVertical: 12,
        fontSize: 20
    },
    askText: {
        fontSize: 20,
        paddingVertical: 7,
        alignSelf: "center"
    },
    foretPassword: {
        fontSize: 20,
        paddingVertical: 7,
        alignSelf: "center",
        paddingBottom: "30%",
        textDecorationLine: 'underline'
    },
    textTitle: {
        fontSize: 20,
        paddingVertical: 7,
        paddingHorizontal: "5%"
    }
});