import React, { useState, useImperativeHandle, useLayoutEffect } from 'react';
import { useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { LineChart } from 'react-native-chart-kit';
import { FlatList } from 'react-native-gesture-handler';
import Colors from '../constraint/Colors';
import host from '../constraint/host';
import postData from '../Util/post';
import i18n from '../constraint/i18n';
import LineChartCus from '../components/LineChartCus';
import ColorBlock from '../components/ColorBlock';

const translator = i18n;
const symptomsCountWeekPHPLink = host + "/pah/AppData/symptomCountWeek.php";
const symptomsCountMonthPHPLink = host + "/pah/AppData/symptomCountMonth.php";
const symptomsCountNoteRPHPLink = host + "/pah/AppData/symptomNoteR.php";
const symptomsCountDetailWeekPHPLink = host + "/pah/AppData/symptomCountDetailWeek.php";
const symptomsCountDetailMonthPHPLink = host + "/pah/AppData/symptomCountDetailMonth.php";
const symptomsCatPHPLink = host + "/pah/AppData/symptomCat.php";
const notesStatPHPLink = host + "/pah/AppData/notesStat.php";
const USERCODE = "1";

const styles = StyleSheet.create({
    headerTitleCell: {
        flex: 1,
        borderWidth: 1,
        borderColor: "#dccbeb",
        alignItems: "center",
        paddingVertical: 12
    }
})

const HeaderClickable = React.forwardRef((props, ref) => {
    const [clicked, setClicked] = useState(false);
    useImperativeHandle(ref, () => ({
        setClicked
    }));
    return (
        <TouchableOpacity
            style={{ ...styles.headerTitleCell, backgroundColor: (clicked ? "#7a67c6" : "white") }}
            onPress={() => {
                props.onClickHandler(props.id, props.title);
            }}>
            <Text style={{ color: (clicked ? "white" : "black"), fontSize: 14 }}>{props.title}</Text>
        </TouchableOpacity>
    );
})

const Past = (props) => {
    const [monthData, setMonthData] = useState([]);
    const [weekData, setWeekData] = useState([]);
    const [weekDetailData, setWeekDetailData] = useState([]);
    const [monthDetailData, setMonthDetailData] = useState([]);
    const [sobjs, setSobjs] = useState([]);
    const fetchWeekData = () => {
        postData(symptomsCountWeekPHPLink, { user_code: USERCODE }).then(json => {
            setWeekData(json);
        });
        postData(symptomsCountDetailWeekPHPLink, { user_code: USERCODE }).then(json => {
            setWeekDetailData(json);
        });
    }
    const fetchMonthData = () => {
        postData(symptomsCountMonthPHPLink, { user_code: USERCODE }).then(json => {
            setMonthData(json);
        });
        postData(symptomsCountDetailMonthPHPLink, { user_code: USERCODE }).then(json => {
            setMonthDetailData(json);
        });
    }
    useEffect(() => {
        fetchMonthData();
        fetchWeekData();
        postData(symptomsCatPHPLink, {}).then(json => {
            setSobjs(json);
        });
    }, []);
    const [weekOrMonth, setWeekOrMonth] = useState(true);
    let weekTitle = ["本週", "上週", ...Array(7).fill(0).map((val, idx) => (idx + 2) + "週前")]
    let monthTitle = ["本月", "上月", ...Array(3).fill(0).map((val, idx) => (idx + 2) + "個月前")]


    return <View style={{ width: "100%", height: 50 }}>
        <View style={{ flexDirection: "row", justifyContent: "space-evenly", paddingVertical: 7, borderBottomWidth: 1, borderBottomColor: "#ddd", marginBottom: 10 }}>
            {sobjs.slice(0, 3).map((sobj) => {
                return (<View style={{ flexDirection: "row" }}>
                    <View style={{ height: 15, width: 15, backgroundColor: sobj.color }} />
                    <Text>{sobj.scat_title_zh}</Text>
                </View>
                );
            })}
        </View>
        <View style={{ flexDirection: "row", paddingHorizontal: 12, marginBottom: 5, justifyContent: "space-between" }}>
            <Text>
                點擊「週」及「月」可查看症狀紀錄的變化{"\n"}
                「基本情報」可查體重及血氧飽和度紀錄{"\n"}
                當你點擊圖表時，可查看症狀詳情
            </Text>
            <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                    onPress={() => {
                        setWeekOrMonth(true);
                    }}
                    style={{ borderWidth: 1, height: 26, width: 26, padding: 5, margin: 2 }}>
                    <Text>週</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        setWeekOrMonth(false);
                    }}
                    style={{ borderWidth: 1, height: 26, width: 26, padding: 5, margin: 2 }}>
                    <Text>月</Text>
                </TouchableOpacity>
            </View>
        </View>
        {weekOrMonth ? weekData.map((val, idx) => {
            return (<View style={{ flexDirection: "row", paddingHorizontal: 7 }}>
                <View style={{ flex: 1, height: 52, justifyContent: "center", alignItems: "center", borderRightWidth: 1, borderRightColor: "#ddd" }}>
                    <Text>{weekTitle[idx]}</Text>
                </View>
                <View style={{ flexDirection: "row", flex: 6 }}>
                    {sobjs.map((sobj, s_index) => {
                        let record = val.find((item) => item.category_zh == sobj.scat_title_zh);

                        return (
                            (record != null && weekDetailData.length != 0) ?
                                <ColorBlock data={weekDetailData[idx]} unit={"week"} color={sobj.color} nthPeroid={idx} category={record.s_category} flexAmount={record.amount} />
                                :
                                <>

                                </>
                        )
                    })}
                </View>
            </View>);
        }) : monthData.map((val, idx) => {
            return (<View style={{ flexDirection: "row", paddingHorizontal: 7 }}>
                <View style={{ flex: 1, height: 52, justifyContent: "center", alignItems: "center", borderRightWidth: 1, borderRightColor: "#ddd" }}>
                    <Text>{monthTitle[idx]}</Text>
                </View>
                <View style={{ flexDirection: "row", flex: 6 }}>
                    {sobjs.map((sobj, s_index) => {
                        let record = val.find((item) => item.category_zh == sobj.scat_title_zh);

                        return (
                            (record != null && monthDetailData.length != 0) ?
                                <ColorBlock data={monthDetailData[idx]} unit={"month"} color={sobj.color} nthPeroid={idx} category={record.s_category} flexAmount={record.amount} />
                                :
                                <>

                                </>
                        )
                    })}
                </View>
            </View>);
        })}
    </View >
}
const BasicInfo = (props) => {
    const daysInMonthCur = (offset = 0) => { //Cur for current
        return new Date((new Date()).getFullYear(), (new Date()).getMonth() + offset, 0).getDate();
    }
    const daysInYearCur = (offset = 0) => {
        return (new Date()).getFullYear() % 400 === 0 || ((new Date()).getFullYear() % 100 !== 0 && (new Date()).getFullYear() % 4 === 0) ? 366 : 365;
    }
    const check = (arr, code) => {
        return arr.length == 0 ? [0] : arr;
    }
    const finder = (item, code) => {
        let object = item.record.find(record => record.symptom_code == code);
        if (object != null) {
            return parseInt(object.remarks);
        } else {
            return 0;
        }
    }
    const [data, setData] = useState([]);
    const [widthRatio, setWidthRatio] = useState(0);
    const dateFormat = (data) => ({
        "2WEEK": {
            data: Array(14).fill(0).map((_, index) => {
                let datePeroid = new Date();
                datePeroid.setDate(datePeroid.getDate() - index);
                let record = data.filter(item => {
                    return new Date(item.date).toDateString() == datePeroid.toDateString()
                });
                return { dateLabel: (datePeroid.getUTCMonth() + 1) + "/" + datePeroid.getDate(), record };
            }),
            width: 350 / 14
        },
        "1MONTH": {
            data: Array(daysInMonthCur()).fill(0).map((_, index) => {
                let d = new Date();
                d.setDate(d.getDate() - index);
                let record = data.filter(item => {
                    return new Date(item.date).toDateString() == d.toDateString()
                });
                return { dateLabel: d.getDay() == 1 || d.getTime() == (new Date()).getTime() ? (d.getUTCMonth() + 1) + "/" + d.getDate() : "", record };
            }),
            width: 350 / daysInMonthCur()
        },
        "2MONTH": {
            data: Array(daysInMonthCur() + daysInMonthCur(-1) + 1 /* the "+1" for padding, label will be cut a part if without it */).fill(0).map((_, index) => {
                let d = new Date();
                d.setDate(d.getDate() - index);
                let record = data.filter(item => {
                    return new Date(item.date).toDateString() == d.toDateString()
                });
                console.log(record)
                return { dateLabel: d.getDay() == 1 || d.getTime() == (new Date()).getTime() ? (d.getUTCMonth() + 1) + "/" + d.getDate() : "", record };
            }),
            width: 350 / daysInMonthCur()
        },
        "1YEAR": {
            data: Array(Math.floor(daysInYearCur())).fill(0).map((_, index) => {
                let d = new Date();
                d.setDate(d.getDate() - index);
                let record = data.filter(item => {
                    return new Date(item.date).toDateString() == d.toDateString()
                });
                return { dateLabel: d.getDate() == 1 || d.getTime() == (new Date()).getTime() ? (d.getUTCMonth() + 1) + "/" + d.getDate() : "", record };
            }),
            width: 350 / daysInMonthCur()
        }
    });
    const fetchData = (interval, unit) => {
        let range = "" + Math.abs(interval) + "" + unit;
        postData(notesStatPHPLink, { interval, unit, user_code: USERCODE }).then((json) => {
            setData(dateFormat(json)[range.toUpperCase()]['data']);
            setWidthRatio(dateFormat(json)[range.toUpperCase()]['width']);
        });
    }
    // console.log("aaaaaaaaaaaa", data.filter(item => item['symptom_code'] == 'S15').map(item => parseInt(item.remarks)))

    React.useLayoutEffect(() => {
        fetchData(-2, "WEEK");
    }, []);
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState("");
    const [items, setItems] = useState([
        { label: '2星期', value: '2WEEK' },
        { label: '1個月', value: '1MONTH' },
        { label: '2個月', value: '2MONTH' },
        { label: '1年', value: '1YEAR' },
    ]);
    const onValueChange = (value) => {
        setValue(value());
        switch (value()) {
            case '2WEEK': fetchData(-2, "WEEK"); break;
            case '1MONTH': fetchData(-1, "MONTH"); break;
            case '2MONTH': fetchData(-2, "MONTH"); break;
            case '1YEAR': fetchData(-1, "YEAR"); break;
        }
    }
    return <View>
        <View style={{ zIndex: 99999, flexDirection: "row", paddingHorizontal: 12, marginBottom: 10, marginTop: 5 }}>
            <Text>
                點擊「週」及「月」可查看症狀紀錄的變化{"\n"}
                「基本情報」可查體重及血氧飽和度紀錄{"\n"}
                當你點擊圖表時，可查看症狀詳情

            </Text>
            <View style={{ justifyContent: "center" }}>
                <DropDownPicker
                    style={{
                        width: 100, height: 20
                    }}
                    placeholder={"2星期"}
                    placeholderStyle={{
                        color: "grey",
                        fontSize: 12
                    }}
                    open={open}
                    value={value}
                    items={items}
                    setOpen={setOpen}
                    setValue={onValueChange}
                    setItems={setItems}
                />
            </View>
        </View>
        <Text style={{
            color: "#c34d7a",
            fontWeight: "bold",
            fontSize: 17,
            width: "100%",
            textAlign: "center",
            paddingVertical: 5,
            backgroundColor: "#fdecf0"
        }}>體重</Text>
        <LineChartCus colorString={"180, 38, 93"}
            width={data.length * widthRatio}
            yAxisLabel={"kg"}
            data={[{
                data: check(data.map(item => finder(item, "S14"))),
                color: (opacity = 0) => `rgba(180, 38, 93, 1)`
            }]}
            labels={data.map(item => item.dateLabel)} />
        <Text style={{
            color: "#3a5e86",
            fontWeight: "bold",
            fontSize: 17,
            width: "100%",
            textAlign: "center",
            paddingVertical: 5,
            backgroundColor: "#d9f4fe"
        }}>血氧飽和度</Text>
        <LineChartCus colorString={"180, 38, 93"}
            width={data.length * widthRatio}
            yAxisLabel={"%"}
            data={[{
                data: check(data.map(item => finder(item, "S16"))),
                color: (opacity = 0) => `rgba(25, 63, 110, 1)`
            }]}
            labels={data.map(item => item.dateLabel)} />
    </View>
}
const SpeciInfo = (props) => {
    const [noteData, setNoteData] = useState([]);
    const fetchNoteData = () => {
        postData(symptomsCountNoteRPHPLink, { user_code: USERCODE }).then(json => {
            setNoteData(json);
        });
    }
    useLayoutEffect(() => {
        fetchNoteData();
    }, []);
    return <FlatList
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ margin: 10 }}
        data={noteData.filter(item => item.s_title_zh == null)}
        renderItem={({ item, index }) => {
            return <View style={{ flexDirection: "row", width: "100%" }}>
                <View style={{ flex: 3, borderWidth: 1, borderColor: "#aaa", paddingHorizontal: 3, paddingVertical: 5 }}><Text style={{ textAlign: 'center' }}>{translator.dayFunc(item.date).slice(0, 5) + "\n" + translator.dayFunc(item.date).slice(5)}</Text></View>
                <View style={{ flex: 4, borderWidth: 1, borderColor: "#aaa", paddingHorizontal: 3, paddingVertical: 5 }}><Text>{item.s_title_zh == null ? "特別事項" : item.s_title_zh}</Text></View>
                <View style={{ flex: 10, borderWidth: 1, borderColor: "#aaa", paddingHorizontal: 3, paddingVertical: 5 }}><Text>{item.remarks}</Text></View>
            </View>
        }} />
}
const PastRecord = (props) => {
    const [headerID, setHeaderID] = useState(0);
    const page = [
        <Past />,
        <BasicInfo />,
        <SpeciInfo />
    ];
    let headerClickableRef = [];
    const headerClick = (id, title = "N/A") => {
        headerClickableRef.forEach(ref => {
            if (ref != null) {
                ref.setClicked(false);
            }
        });
        headerClickableRef[id].setClicked(true);
        setHeaderID(id);
    }
    useEffect(() => {
        headerClick(0);
    }, []);
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: "row" }}>
                {["症狀紀錄", "基本情報", "特別事項", "下載紀錄"].map((title, index) => (
                    <HeaderClickable
                        ref={ref => headerClickableRef.push(ref)}
                        title={title} onClickHandler={headerClick}
                        id={index} />
                ))}
            </View>
            {page[headerID]}
        </View>
    );
}
export default PastRecord;