import * as React from 'react';
import {
    View, Text, StyleSheet, FlatList,
    Image, TouchableOpacity, Modal,
    TouchableHighlight, TextInput
} from 'react-native';
import Colors from '../constraint/Colors';
import host from '../constraint/host';
import imgForSymptoms from '../constraint/imgForSymptoms';
import postData from '../Util/post';
import i18n from "../constraint/i18n";

const translator = i18n;
const symptomsListPHPLink = host + "/pah/AppData/symptoms.php";
const symptomsCUDPHPLink = host + "/pah/AppData/symptomsCUD.php";//for create/update/delete
const symptomsRPHPLink = host + "/pah/AppData/symptomsR.php";//for read only
const symptomsNotePHPLink = host + "/pah/AppData/symptomNote.php";//for create/update/delete
const imgLink = host + "/pah/";
const USERCODE = "1";

export default function TodaySymptoms(props) {
    const numColumns = 2;
    const notClickableCount = 0;
    const [currentDate, setCurrentDate] = React.useState(new Date);
    const [record, setRecord] = React.useState([]);
    // const [selectionList, setSelectionList] = React.useState([]);
    const [modalVisible, setModalVisible] = React.useState(false);
    const [remarkDetail, setRemarkDetail] = React.useState({});
    const [originRecord, setOriginRecord] = React.useState([]);
    const flatlistRef = React.useRef();
    const [refresh, setRefresh] = React.useState(false);
    const showModal = (remark) => {
        setRemarkDetail(remark);
        setModalVisible(true);
    }
    const updateSelected = (y, x) => {
        let tempRecord = record;
        console.log(y, x);
        tempRecord[y][x]['selected'] = !tempRecord[y][x]['selected'];
        setRecord(tempRecord);
        console.log(tempRecord[y][x]['selected']);
        setRefresh(!refresh);
    }
    const updateNote = (y, x, text) => {
        let tempRecord = record;
        console.log(y, x);
        tempRecord[y][x]['text'] = text;
        setRecord(tempRecord);
        setRefresh(!refresh);
    }
    const refreshSelectionByDay = (date_, num, modifiedRecord) => {
        let date = new Date(date_);
        date.setDate(date.getDate() + num);
        setCurrentDate(date);
        postData(symptomsRPHPLink, {
            date: date.toISOString().substring(0, 10),
            user_code: USERCODE
        }).then(json => {
            if (Array.isArray(json) || json == null) {
                let selection = modifiedRecord.map(
                    subRecord => subRecord.map(
                        item => {
                            if (item.type != 'text') {
                                item['selected'] = false;
                                item['text'] = "";
                                if (json != null) {
                                    let obj = json.find(obj => obj.symptom_code == item.code);
                                    if (obj != null) {
                                        if (obj.remarks == null) {
                                            item['selected'] = true;
                                        } else {
                                            item['text'] = obj.remarks;
                                        }
                                    }
                                }
                            }
                            return item;
                        }));
                console.log(selection)
                setRecord(selection);
                setRefresh(!refresh);
                // setSelectionList(selection);
            }
        }).catch(err => { throw err; });
    }
    const minusOneDay = () => {
        refreshSelectionByDay(currentDate, -1, record);
    }
    const plusOneDay = () => {
        refreshSelectionByDay(currentDate, 1, record);
    }
    const saveRecord = async () => {
        let err = await postData(symptomsCUDPHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            scodes: [].concat(...record)
                .filter(item =>
                    item.type != 'text')
                .map(item =>
                    ({ code: item.code, value: item.selected }))
                .filter(item => item.value)
                .map(item => item.code)
        }).then(() => null, err => err);
        if (err == null) {
            console.log("success: " + symptomsCUDPHPLink);
        }
        await postData(symptomsNotePHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            scode: [].concat(...record)
                .filter(item =>
                    item.type != 'text')
                .filter(item =>
                    item.text != "")
                .map(item =>
                    item.code),
            svalue: [].concat(...record)
                .filter(item =>
                    item.type != 'text')
                .filter(item =>
                    item.text != "")
                .map(item =>
                    item.text)
        }).then(() => null, err => err);
        if (err == null) {
            console.log("success: " + symptomsNotePHPLink);
        }
    }
    const modifyData = (data) => {
        // const addBannerIndex = data.length-3;
        let dataMaxCat = data[data.length - 1].s_category;
        const arr = [];
        for (let i = 0; i < dataMaxCat + 1; i++) {
            var tmp = [];
            let currentData = data.filter(item => item.s_category == i);
            if (currentData.length != 0) {
                arr.push([{ type: 'text', clickable: false, text: i }]);
                currentData.forEach((val, index) => {
                    if (index % numColumns == 0 && index != 0) {
                        arr.push(tmp);
                        tmp = [];
                    }
                    // if (index == addBannerIndex) {
                    //     arr.push([{ type: 'other' }]);
                    //     tmp = [];
                    // }
                    tmp.push({ ...val, selected: false, text: "" });
                });
                arr.push(tmp);
            }
        }
        return arr;
    }
    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };
    const [topMoreVisible, setTopMoreVisible] = React.useState(false);
    const [bottomMoreVisible, setBottomMoreVisible] = React.useState(true);
    const renderFunc = (item, index) => {
        if (item[0].type == 'text') {
            return (
                <></>
                // <View style={{ width: "100%", alignItems: "center" }}>
                //     <View style={{ backgroundColor: "#eeeded", width: "80%", alignItems: "center", paddingVertical: 8, borderRadius: 20 }}>
                //         <Text style={{ color: "#807bb4", fontSize: 20 }}>{item[0].text}</Text>
                //     </View>
                // </View>
            )
        }
        console.log(index)
        const columns = item.map((val, idx) => {
            return (
                <View style={{ width: "50%", paddingVertical: 10, justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => {
                        if (val.s_category != 4) {
                            updateSelected(index, idx);
                            console.log(record[index][idx].selected);
                        }
                        else {
                            showModal({ title: val.s_title_zh, y: index, x: idx });
                        }
                    }
                    }>
                        {record[index][idx].selected ?
                            <Image source={{ uri: imgLink + val.s_icon_active }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                            :
                            <Image source={{ uri: imgLink + val.s_icon }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                        }
                    </TouchableOpacity>
                    <Text style={{ alignSelf: "center", fontSize: 18, paddingVertical: 6 }}>{val.s_title_zh}</Text>
                </View>)
        });
        return (
            <View key={index} style={{ width: "100%", flexDirection: 'row', marginBottom: 10 }}>
                {columns}
            </View>);
    }
    React.useEffect(() => {
        fetch(symptomsListPHPLink)
            .then(response => response.json())
            .then(json => {
                let sorted = json.sort((a, b) => {
                    if (a.s_category < b.s_category) return -1;
                    if (a.s_category > b.s_category) return 1;
                    return 0;
                }).sort((a, b) => {
                    if (a.s_category == b.s_category) {
                        if (a.ordering < b.ordering) return -1;
                        if (a.ordering > b.ordering) return 1;
                        return 0;
                    }
                    return 0;
                }).filter(item => item.isShow == "1");
                setOriginRecord(sorted);
                let modified = modifyData(sorted);
                setRecord(modified);
                refreshSelectionByDay((new Date), 0, modified);
                // setSelectionList(Array(json.length - notClickableCount).fill(false));
            })
            .catch(err => console.log("9", err));
    }, []);
    return (<View style={styles.container}>
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(!modalVisible);
            }}
        >
            <TouchableHighlight onPress={() => {
                setModalVisible(!modalVisible)
            }}>
                <View style={{ backgroundColor: "rgba(0, 0, 0, 0.5)", width: "100%", height: "100%" }}>
                    <View style={{ backgroundColor: "white", marginVertical: "40%", marginHorizontal: 5, height: "60%", borderRadius: 10, padding: 10 }}>
                        <View style={{ flexDirection: "row", width: "100%", justifyContent: "space-between" }}>
                            <View style={{ padding: 5, flexDirection: "row" }}>
                                <TouchableOpacity onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                    <Text>✖</Text>
                                </TouchableOpacity>
                                <Text>Delete</Text>
                            </View>
                            <TouchableOpacity
                                style={{ alignSelf: "flex-end", borderColor: Colors.gray, borderRadius: 5, borderWidth: 1, padding: 5 }}
                                onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                <Text>確定</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}>
                            <Text style={{ fontSize: 25, padding: 5 }}>
                                {remarkDetail.title}
                            </Text>
                        </View>
                        {remarkDetail.title === "特別事項記錄" ?
                            <TextInput
                                style={{ margin: 5 }}
                                multiline={true}
                                numberOfLines={4}
                                placeholder={"請輸入你要記錄的內容（最多100個字符）"} />
                            :
                            <View style={{ flexDirection: "row" }}>
                                <TextInput
                                    onChangeText={(text) => { remarkDetail == null ? () => { } : updateNote(remarkDetail.y, remarkDetail.x, text) }}
                                    value={record[remarkDetail.y] != null ? record[remarkDetail.y][remarkDetail.x].text : ""}
                                    style={{ margin: 5, borderWidth: 1, borderColor: "#ddd", height: 30, fontSize: 20, width: "75%" }} />
                                <Text style={{ margin: 6, height: 30, fontSize: 20 }}>
                                    {
                                        remarkDetail.title === "體重" ?
                                            "kg" :
                                            remarkDetail.title === "發燒" ?
                                                "°C" :
                                                remarkDetail.title === "血氧飽和度" ?
                                                    "%" :
                                                    remarkDetail.title === "心跳" ?
                                                        "bpm" :
                                                        remarkDetail.title === "血壓" ?
                                                            "mmHg" : ""
                                    }
                                </Text>
                            </View>
                        }
                    </View>
                </View>
            </TouchableHighlight>
        </Modal>
        <View style={styles.dateView}>
            <TouchableOpacity onPress={minusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    prev
                </Text>
            </TouchableOpacity>
            <Text style={styles.dateText}>
                {translator.dayFunc(currentDate)}
            </Text>
            <TouchableOpacity onPress={plusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    next
                </Text>
            </TouchableOpacity>
        </View>
        {/* <View style={{ position: 'absolute', top: 40, display: (topMoreVisible) ? "flex" : "none" }}><Text>Up</Text></View> */}
        <View style={{ height: "82%" }}>
            <FlatList data={record} ref={flatlistRef}
                extraData={refresh}
                onScroll={({ nativeEvent }) => {
                    setBottomMoreVisible(true);
                    setTopMoreVisible(true);
                    if (isCloseToBottom(nativeEvent)) {
                        setBottomMoreVisible(false);
                    }
                    if (nativeEvent.contentOffset.y <= 0) {
                        console.log("start");
                        setTopMoreVisible(false);
                    }
                }}
                scrollEventThrottle={400}
                ListHeaderComponent={() => {
                    return (
                        <View style={{ flexDirection: "row", padding: 7 }}>
                            <Text style={{ width: "50%", textAlign: "center" }}>症狀ON</Text>
                            <Text style={{ width: "50%", textAlign: "center" }}>症狀OFF</Text>
                        </View>
                    );
                }}
                ListFooterComponent={() => {
                    return (
                        <View style={{ width: "100%", alignItems: "center", paddingBottom: 20 }}>
                            <TouchableOpacity style={{ width: "80%" }} onPress={() => {
                                showModal({ title: "特別事項記錄" });
                            }}>
                                <View style={{ backgroundColor: "#eeeded", alignItems: "center", paddingVertical: 8, borderRadius: 100 }}>
                                    <Text style={{ color: "#807bb4", fontSize: 20 }}>其他注意事項</Text>
                                </View>
                            </TouchableOpacity>
                        </View>);
                }}
                renderItem={({ item, index }) => {
                    return renderFunc(item, index);
                }} />
        </View>
        <View style={{ position: 'absolute', bottom: 80, display: (!bottomMoreVisible || !topMoreVisible) ? "flex" : "none" }}>
            <TouchableOpacity onPress={() => {
                if (topMoreVisible) {
                    flatlistRef.current.scrollToOffset({ offset: 0, animated: true })
                } else {
                    flatlistRef.current.scrollToEnd({ animating: true });
                }
            }}>
                <Text>{topMoreVisible ? "Up" : "Down"}</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.saveArea}>
            <TouchableOpacity style={styles.btnMore} onPress={saveRecord}>
                <Text style={styles.btnMoreText}>儲存</Text>
            </TouchableOpacity>
        </View>
    </View>);
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "white",
    },
    dateView: {
        flexDirection: "row",
        backgroundColor: "#ddd",
        width: "100%",
        justifyContent: "space-between",
        padding: 7
    },
    dateText: {
        fontSize: 22,
    },
    btnMore: {
        backgroundColor: "#b497c6",
        width: "90%",
        padding: 10,
        marginVertical: 3,
        marginHorizontal: "5%",
        borderRadius: 20,
        alignItems: "center",
    },
    btnMoreText: {
        color: "white",
        fontWeight: "bold",
        fontSize: 23
    },
    saveArea: {
        alignItems: "center",
        padding: 15,
        backgroundColor: "#ddd",
        height: "12%",
        width: "100%"
    }
})