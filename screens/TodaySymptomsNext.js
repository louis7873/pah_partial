import * as React from 'react';
import {
    View, Text, StyleSheet, FlatList,
    Image, TouchableOpacity, Modal,
    TouchableHighlight, TextInput
} from 'react-native';
import Colors from '../constraint/Colors';
import host from '../constraint/host';
import imgForSymptoms from '../constraint/imgForSymptoms';
import postData from '../Util/post';
import i18n from "../constraint/i18n";

const translator = i18n;
const symptomsListPHPLink = host + "/pah/AppData/symptoms.php";
const symptomsCUDPHPLink = host + "/pah/AppData/symptomsCUD.php";//for create/update/delete
const symptomsRPHPLink = host + "/pah/AppData/symptomsR.php";//for read only
const symptomsNotePHPLink = host + "/pah/AppData/symptomNote.php";//for create/update/delete
const imgLink = host + "/pah/";
const USERCODE = "1";

export default function TodaySymptomsNext(props) {
    const flatlistRef = React.useRef();
    const [record, setRecord] = React.useState([]);
    const [currentDate, setCurrentDate] = React.useState(new Date);
    const [modalVisible, setModalVisible] = React.useState(false);
    const [remarkDetail, setRemarkDetail] = React.useState({});
    const [topMoreVisible, setTopMoreVisible] = React.useState(false);
    const [bottomMoreVisible, setBottomMoreVisible] = React.useState(true);
    React.useEffect(() => {
        fetch(symptomsListPHPLink)
            .then(response => response.json())
            .then(json => {
                let sorted =
                    json.sort((a, b) => {
                        if (a.s_category < b.s_category) return -1;
                        if (a.s_category > b.s_category) return 1;
                        return 0;
                    }).sort((a, b) => {
                        if (a.s_category == b.s_category) {
                            if (a.ordering < b.ordering) return -1;
                            if (a.ordering > b.ordering) return 1;
                            return 0;
                        }
                        return 0;
                    }).filter(item => item.isShow == "1");
                let modified = groupData(sorted);
                console.log(modified);
                setRecord(modified);
                refreshSelectionByDay(new Date, modified);
            })
            .catch(err => console.log(symptomsListPHPLink, ":", err));
    }, []);
    const groupData = (data) => {
        // const addBannerIndex = data.length-3;
        let dataMaxCat = data[data.length - 1].s_category;
        const result = [];
        for (let i = 0; i < dataMaxCat + 1; i++) {
            let currentData = data.filter(item => item.s_category == i);
            if (currentData.length != 0) {
                result.push(currentData);
            }
        }
        return result;
    }
    const refreshSelectionByDay = (date_, modifiedRecord, num = 0) => {
        let date = new Date(date_);
        date.setDate(date.getDate() + num);
        setCurrentDate(date);
        postData(symptomsRPHPLink, {
            date: date.toISOString().substring(0, 10),
            user_code: USERCODE
        }).then(json => {
            let selection = modifiedRecord.map(
                subRecord => subRecord.map(
                    item => {
                        item.value = null;
                        if (Array.isArray(json)) {
                            let obj = json.find(obj => obj.symptom_code == item.code);
                            if (obj != null) {
                                if (obj.mode == "boolean") {
                                    item.type = "boolean";
                                    item.value = true;
                                } else if (obj.mode == "text") {
                                    item.type = "text";
                                    item.value = obj.remarks;
                                }
                            }
                        }
                        return item;
                    }));
            setRecord(selection);
        }).catch(err => { throw err; });
    }
    const minusOneDay = () => {
        refreshSelectionByDay(currentDate, record, -1);
    }
    const plusOneDay = () => {
        refreshSelectionByDay(currentDate, record, 1);
    }
    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 10;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };
    const showModal = (remark) => {
        setRemarkDetail(remark);
        setModalVisible(true);
    }
    const updateSelected = (y, x) => {
        let tempRecord = [...record];
        console.log(y, x);
        tempRecord[y][x]['value'] = !tempRecord[y][x]['value'];
        setRecord(tempRecord);
        console.log(tempRecord);
    }
    const updateNote = (y, x, text) => {
        let tempRecord = [...record];
        console.log(y, x);
        tempRecord[y][x]['value'] = text;
        setRecord(tempRecord);
        console.log(tempRecord);
    }
    const saveRecord = async () => {
        let err_symptom = await postData(symptomsCUDPHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            scodes: [].concat(...record)
                .filter(item => item.mode == "boolean")
                .map(item =>
                    ({ code: item.code, value: item.value }))
                .filter(item => item.value)
                .map(item => item.code)
        }).then(() => null, err => err);
        if (err_symptom == null) {
            console.log("success: " + symptomsCUDPHPLink);
        }
        let err_note = await postData(symptomsNotePHPLink, {
            user_code: USERCODE,
            date: currentDate.toISOString().substring(0, 10),
            scode: [].concat(...record)
                .filter(item => item.mode == "text")
                .filter(item => item.value != "")
                .map(item =>
                    item.code),
            svalue: [].concat(...record)
                .filter(item => item.mode == "text")
                .filter(item => item.value != "")
                .map(item =>
                    item.value)
        }).then(() => null, err => err);
        if (err_note == null) {
            console.log("success: " + symptomsNotePHPLink);
        }
    }
    return <View style={{
        flex: 1,
        alignItems: "center",
    }}>
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(!modalVisible);
            }}
        >
            {}
            <TouchableHighlight onPress={() => {
                setModalVisible(!modalVisible)
            }}>
                <View style={{ backgroundColor: "rgba(0, 0, 0, 0.5)", width: "100%", height: "100%" }}>
                    <View style={{ backgroundColor: "white", marginVertical: "40%", marginHorizontal: 5, height: "60%", borderRadius: 10, padding: 10 }}>
                        <View style={{ flexDirection: "row", width: "100%", justifyContent: "space-between" }}>
                            <View style={{ padding: 5, flexDirection: "row" }}>
                                <TouchableOpacity onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                    <Text>✖</Text>
                                </TouchableOpacity>
                                <Text>Delete</Text>
                            </View>
                            <TouchableOpacity
                                style={{ alignSelf: "flex-end", borderColor: Colors.gray, borderRadius: 5, borderWidth: 1, padding: 5 }}
                                onPress={() => {
                                    setModalVisible(!modalVisible)
                                }}>
                                <Text>確定</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}>
                            <Text style={{ fontSize: 25, padding: 5 }}>
                                {remarkDetail.title}
                            </Text>
                        </View>
                        {remarkDetail.title === "特別事項記錄" ?
                            <TextInput
                                style={{ margin: 5 }}
                                multiline={true}
                                numberOfLines={4}
                                placeholder={"請輸入你要記錄的內容（最多100個字符）"} />
                            :
                            <View style={{ flexDirection: "row" }}>
                                <TextInput
                                    onChangeText={(text) => { remarkDetail == null ? () => { } : updateNote(remarkDetail.catID, remarkDetail.itemID, text) }}
                                    value={record[remarkDetail.catID] != null ? record[remarkDetail.catID][remarkDetail.itemID].value : ""}
                                    style={{ margin: 5, borderWidth: 1, borderColor: "#ddd", height: 30, fontSize: 20, width: "75%" }} />
                                <Text style={{ margin: 6, height: 30, fontSize: 20 }}>
                                    {
                                        remarkDetail.suffix
                                    }
                                </Text>
                            </View>
                        }
                    </View>
                </View>
            </TouchableHighlight>
        </Modal>
        <View style={styles.dateView}>
            <TouchableOpacity onPress={minusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    prev
                </Text>
            </TouchableOpacity>
            <Text style={styles.dateText}>
                {translator.dayFunc(currentDate)}
            </Text>
            <TouchableOpacity onPress={plusOneDay}>
                <Text style={{ fontSize: 15, padding: 3 }}>
                    next
                </Text>
            </TouchableOpacity>
        </View>
        <FlatList data={record}
            ref={flatlistRef}
            onScroll={({ nativeEvent }) => {
                setBottomMoreVisible(true);
                setTopMoreVisible(true);
                if (isCloseToBottom(nativeEvent)) {
                    setBottomMoreVisible(false);
                }
                if (nativeEvent.contentOffset.y <= 0) {
                    console.log("start");
                    setTopMoreVisible(false);
                }
            }}
            scrollEventThrottle={400}
            ListHeaderComponent={() => {
                return (
                    <View style={{ flexDirection: "row", padding: 7 }}>
                        <Text style={{ width: "50%", textAlign: "center" }}>症狀ON</Text>
                        <Text style={{ width: "50%", textAlign: "center" }}>症狀OFF</Text>
                    </View>
                );
            }}
            renderItem={({ item, index }) => {
                return <FlatList numColumns={2} data={item} renderItem={(subitem) => {
                    return <TouchableOpacity onPress={() => {
                        if (subitem.item.mode != "text") {
                            updateSelected(index, subitem.index);
                        } else {
                            showModal({
                                title: subitem.item.s_title_zh,
                                catID: index,
                                itemID: subitem.index,
                                suffix: subitem.item.suffix
                            });
                        }
                    }}
                        style={{ flex: 1 / 2 }}
                    >
                        <View >
                            {subitem.item.mode == "text" ?
                                <>
                                    <Image source={{ uri: imgLink + subitem.item.s_icon }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                    <Text style={{ alignSelf: "center" }}>{subitem.item.s_title_zh}</Text>
                                </> :
                                (subitem.item.mode == "boolean" &&
                                    <>
                                        {
                                            record[index][subitem.index].value ?
                                                <Image source={{ uri: imgLink + subitem.item.s_icon_active }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                                :
                                                <Image source={{ uri: imgLink + subitem.item.s_icon }} resizeMode="contain" style={{ height: 100, marginVertical: 5, width: 100, alignSelf: "center" }} />
                                        }
                                        <Text style={{ alignSelf: "center" }}>{subitem.item.s_title_zh}</Text>
                                    </>)
                            }
                        </View>
                    </TouchableOpacity>
                }

                } />
            }}
            ListFooterComponent={() => {
                return (
                    <View style={{ width: "100%", alignItems: "center", paddingBottom: 20 }}>
                        <TouchableOpacity style={{ width: "80%" }} onPress={() => {
                            showModal({ title: "特別事項記錄" });
                        }}>
                            <View style={{ backgroundColor: "#eeeded", alignItems: "center", paddingVertical: 8, borderRadius: 100 }}>
                                <Text style={{ color: "#807bb4", fontSize: 20 }}>其他注意事項</Text>
                            </View>
                        </TouchableOpacity>
                    </View>);
            }} />
        <View style={{ position: 'absolute', bottom: 80, display: (!bottomMoreVisible || !topMoreVisible) ? "flex" : "none" }}>
            <TouchableOpacity onPress={() => {
                if (topMoreVisible) {
                    flatlistRef.current.scrollToOffset({ offset: 0, animated: true })
                } else {
                    flatlistRef.current.scrollToEnd({ animating: true });
                }
            }}>
                <Text>{topMoreVisible ? "Up" : "Down"}</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.saveArea}>
            <TouchableOpacity style={styles.btnMore} onPress={saveRecord}>
                <Text style={styles.btnMoreText}>儲存</Text>
            </TouchableOpacity>
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        backgroundColor: "white",
    },
    dateView: {
        flexDirection: "row",
        backgroundColor: "#ddd",
        width: "100%",
        justifyContent: "space-between",
        padding: 7
    },
    dateText: {
        fontSize: 22,
    },
    btnMore: {
        backgroundColor: "#b497c6",
        width: "90%",
        padding: 10,
        marginVertical: 3,
        marginHorizontal: "5%",
        borderRadius: 20,
        alignItems: "center",
    },
    btnMoreText: {
        color: "white",
        fontWeight: "bold",
        fontSize: 23
    },
    saveArea: {
        alignItems: "center",
        padding: 15,
        backgroundColor: "#ddd",
        height: "12%",
        width: "100%"
    }
})