import * as React from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Modal,
    TextInput
} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Colors from "../constraint/Colors";
import { followUpRecord } from "../Data/TestingFollowUpRecordDra";
import i18n from "../constraint/i18n";

const translator = i18n;
export default function FollowUpRecord(props) {
    const getDate = (str) => {
        return new Date(str);
    }
    const [record, setRecord] = React.useState([]);
    const [modalVisible, setModalVisible] = React.useState(false);
    const [remarkDetail, setRemarkDetail] = React.useState({});
    const showModal = (remark) => {
        console.log(JSON.stringify(remark));
        setRemarkDetail(remark);
        setModalVisible(true);
    }
    const sortRecord = (record_) => {
        let newRecord = record_.sort(function (pres, next) {
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(next.date) - new Date(pres.date);
        });
        let today = new Date();
        newRecord = [
            {
                date:
                    today.getFullYear() + "/" +
                    (today.getMonth() + 1) + "/" +
                    today.getDate()
            }
            , ...newRecord];
        return newRecord;
    }
    React.useEffect(() => {
        setRecord(sortRecord(followUpRecord));
    }, []);
    return (
        <View style={styles.container}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}
            >
                <TouchableHighlight onPress={() => {
                    setModalVisible(!modalVisible)
                }}>
                    <View style={{ backgroundColor: "rgba(0, 0, 0, 0.5)", width: "100%", height: "100%" }}>
                        <View style={{ backgroundColor: "white", marginVertical: "40%", marginHorizontal: 5, height: "60%", borderRadius: 10, padding: 10 }}>
                            <View style={{ flexDirection: "row", width: "100%", justifyContent: "space-between" }}>
                                <View style={{ padding: 5, flexDirection: "row" }}>
                                    <TouchableOpacity onPress={() => {
                                        setModalVisible(!modalVisible)
                                    }}>
                                        <Text>✖</Text>
                                    </TouchableOpacity>
                                    <Text>{translator.FollowUpRecord_Delete}</Text>
                                </View>
                                <TouchableOpacity
                                    style={{ alignSelf: "flex-end", borderColor: Colors.gray, borderRadius: 5, borderWidth: 1, padding: 5 }}
                                    onPress={() => {
                                        setModalVisible(!modalVisible)
                                    }}>
                                    <Text>{translator.FollowUpRecord_ConfirmText}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ borderBottomWidth: 1, borderBottomColor: "#ddd" }}>
                                <Text style={{ fontSize: 20, padding: 5 }}>覆診備忘錄({
                                    remarkDetail.date &&
                                    (translator.dayFuncWithOutYear(remarkDetail.date))})
                                </Text>
                            </View>
                            <TextInput
                                style={{ margin: 5 }}
                                multiline={true}
                                numberOfLines={4}
                                placeholder={translator.FollowUpRecord_Placeholder}
                                value={remarkDetail.remark} />
                        </View>
                    </View>
                </TouchableHighlight>
            </Modal>
            <FlatList
                style={{
                    width: "100%", borderWidth: 0.5,
                    borderColor: "#ddd"
                }}
                ListHeaderComponent={() => {
                    return (
                        <View style={{ flexDirection: "row" }}>
                            <View style={styles.followUpTitleDate}>
                                <Text style={{ color: "#666" }}>{translator.FollowUpRecord_followUpTitleDate}</Text>
                            </View>
                            <View style={styles.followUpTitleRemark}>
                                <Text style={{ color: "#666" }}>{translator.FollowUpRecord_followUpTitleRemark}</Text>
                            </View>
                        </View>
                    );
                }}
                data={record}
                renderItem={
                    ({ item, index }) => {
                        let date = getDate();
                        return (
                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.dateCellValue}>
                                    <Text style={{ color: "#b497c6", fontWeight: "bold" }}>{translator.dayFuncWithOutYear(item.date)}</Text>
                                </View>
                                <View style={styles.remakeCellValue}>
                                    <View style={{ margin: 5, flex: 9 }}>
                                        {index == 0 ?
                                            <TouchableOpacity style={styles.btnNewDateRecord} onPress={() => {
                                                props.navigation.navigate("FollowUpDate", { defaultDate: record[0].date });
                                            }}>
                                                <Text style={styles.btnNewDateRecordText}>{translator.FollowUpRecord_btnNewDateRecord}</Text>
                                            </TouchableOpacity> :
                                            item.remark != null ?
                                                <Text style={{ color: "#666", fontSize: 13, flexWrap: "wrap" }}>
                                                    {item.remark}
                                                </Text> :
                                                <TouchableOpacity style={styles.btnCreate} onPress={() => {
                                                    showModal(item);
                                                }}>
                                                    <Text style={styles.btnCreateText}>{translator.FollowUpRecord_btnCreateText}</Text>
                                                </TouchableOpacity>}
                                    </View>
                                    {item.remark != null && index != 0 &&
                                        <TouchableOpacity onPress={() => {
                                            showModal(item);
                                        }}>
                                            <View style={{ margin: 5, flex: 2 }}>
                                                <View style={{ height: 35, width: 35, backgroundColor: "#ddd" }}></View>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        );
                    }} />
        </View>
    );
}

const styles = StyleSheet.create({
    btnNewDateRecord: {
        borderColor: "#b497c6",
        borderWidth: 1,
        backgroundColor: "white",
        padding: 8,
        borderRadius: 10,
        alignItems: "center",
        width: "85%",
        alignSelf: "center"
    },
    btnNewDateRecordText: {
        color: "#b497c6",
    },
    btnCreate: {
        backgroundColor: "#b497c6",
        padding: 8,
        borderRadius: 10,
        alignItems: "center",
        width: "85%",
        alignSelf: "center"
    },
    btnCreateText: {
        color: "white",
        fontWeight: "bold"
    },
    remakeCellValue: {
        flex: 3,
        borderWidth: 0.5,
        padding: 3,
        borderColor: "#ddd",
        flexDirection: "row"
    },
    dateCellValue: {
        paddingVertical: 18,
        flex: 1,
        borderWidth: 0.5,
        padding: 3,
        alignItems: "center",
        borderColor: "#ddd",
        backgroundColor: "#eee8f2"
    },
    followUpTitleRemark: {
        flex: 3,
        borderWidth: 0.5,
        padding: 3,
        alignItems: "center",
        borderColor: "#ddd"
    },
    followUpTitleDate: {
        flex: 1,
        borderWidth: 0.5,
        padding: 3,
        alignItems: "center",
        borderColor: "#ddd"
    },
    hintDateTitle: {
        color: "#58b1ce",
        fontWeight: "bold"
    },
    hintDate: {
        color: "#555",
        fontSize: 30
    },
    hintDateArea: {
        flex: 7,
        marginHorizontal: 7
    },
    img: {
        flex: 3,
        backgroundColor: "gray",
        marginHorizontal: 7
    },
    container: {
        alignItems: "center",
        backgroundColor: "white",
    },
    section: {
        backgroundColor: "#f8f8f8",
        borderRadius: 5,
        width: "90%",
        padding: 10,
        marginBottom: 15,
        shadowColor: "#ddd",
        shadowOpacity: 1,
        shadowOffset: {
            width: 3,
            height: 3
        },
        alignItems: "center",
    }
})