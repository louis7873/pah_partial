export default function postData(url, data) {
    let formBody = [];
    for (let property in data) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(data[property]);
        if (Array.isArray(data[property])) {
            data[property].forEach(item => {
                formBody.push(encodedKey + "[]=" + item);
            })
        } else {
            formBody.push(encodedKey + "=" + encodedValue);
        }
    }
    let params = formBody.join("&");
    console.log(url + "?" + params);
    return fetch(url, {
        body: params,
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'user-agent': 'Mozilla/4.0 MDN Example',
            'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        referrer: 'no-referrer',
    })
        .then(response => response.json())
}
