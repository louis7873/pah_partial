<?php

include("Connection/db.php");
if ($_POST["user_code"] != null) {
    for ($i = 0; $i < 5; $i++) {
        $data = [
            "fromTime" => 0 - $i,
            "toTime" => 0 - $i - 1,
            "user_code" => $_POST["user_code"]
        ];
        $sql =
            "SELECT `user_symptom_dairy`.`symptom_code`,
        `symptom_category`.`id`,
        COUNT(`user_symptom_dairy`.`symptom_code`) as 'code_count', 
        `symptom_list`.`s_title_zh`, 
        `symptom_list`.`s_title_en` 
        FROM `user_symptom_dairy`, `symptom_list`,`symptom_category` 
        WHERE `user_symptom_dairy`.`symptom_code`=`symptom_list`.`code` 
        AND `user_symptom_dairy`.`date` < DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY), INTERVAL :fromTime MONTH) 
        AND `user_symptom_dairy`.`date` >= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY), INTERVAL :toTime MONTH) 
        AND `user_symptom_dairy`.`category` = 'Symptom' 
        AND `symptom_list`.`s_category` = `symptom_category`.`id` 
        AND `user_symptom_dairy`.`user_code`=:user_code 
        GROUP BY `user_symptom_dairy`.`symptom_code`";

        // AND `symptom_category`.`id`=:id 
        $stmt = $con->prepare($sql);
        $stmt->execute($data);
        $symptoms_arr = array();
        while ($row = $stmt->fetch()) {
            $symptoms_arr[] = $row;
        }
        $symptoms_arrs[] = $symptoms_arr;
    }
    echo json_encode($symptoms_arrs, JSON_UNESCAPED_UNICODE);
}
