<?php

include("Connection/db.php");

if ($_POST['user_code'] != null) {
    $date = $_POST['date'] == null ? date("Y-m-d") : $_POST['date'];
    $data = ['user_code' => $_POST['user_code'], 'date' => $date];
    $sql = 
        "SELECT * FROM `user_symptom_dairy`,`symptom_list` 
            where user_code=:user_code 
            and date=:date 
            and `symptom_list`.`code` =`user_symptom_dairy`.`symptom_code`";

    $stmt = $con->prepare($sql);
    $stmt->execute($data);
    while ($row = $stmt->fetch()) {
        $obj['symptom_code']=$row['symptom_code'];
        $obj['remarks']=$row['remarks'];
        $obj['mode']=$row['mode'];
        $symptoms_arr[] = $obj;
    }
    echo json_encode($symptoms_arr, JSON_UNESCAPED_UNICODE);
} else {
    echo "{\"status\":\"failed\"}";
}
