<?php
include("Connection/db.php");

$symptoms_arrs = array();
for ($i = 0; $i < 9; $i++) {
    $data = ["user_code" => $_POST['user_code'], "peroid" => (0 - $i), "peroid_next" => (0 - $i - 1)];
    $sql =
        "SELECT COUNT(`symptom_list`.`s_category`) as amount, 
        `symptom_category`.`scat_title_zh` as category_zh, 
        `symptom_category`.`scat_title_en` as category_en, 
        `symptom_list`.`s_category` as category_id
        FROM `user_symptom_dairy`,`symptom_list`,`symptom_category` 
        WHERE `user_symptom_dairy`.`date` < DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY), INTERVAL :peroid WEEK) 
        AND `user_symptom_dairy`.`date` >= DATE_ADD(DATE_ADD(CURDATE(), INTERVAL - WEEKDAY(CURDATE()) DAY), INTERVAL :peroid_next WEEK) 
        AND `symptom_list`.`code` = `user_symptom_dairy`.`symptom_code` 
        AND `user_symptom_dairy`.`category` = 'Symptom' 
        AND `symptom_list`.`s_category` = `symptom_category`.`id` 
        AND `user_symptom_dairy`.`user_code`=:user_code 
        AND `symptom_category`.`chart`='bar'
        GROUP BY `symptom_list`.`s_category` 
        ORDER BY `symptom_list`.`s_category`";
    $stmt = $con->prepare($sql);
    $stmt->execute($data);
    $symptoms_arr = array();
    // echo $sql;
    while ($row = $stmt->fetch()) {
        $obj['amount'] = $row['amount'];
        $obj['category_zh'] = $row['category_zh'];
        $obj['category_en'] = $row['category_en'];
        $obj['s_category'] = $row['category_id'];
        $symptoms_arr[] = $obj;
    }
    $symptoms_arrs[] = $symptoms_arr;
}
echo json_encode($symptoms_arrs, JSON_UNESCAPED_UNICODE);
