<?php

include("Connection/db.php");

function getAllSymptoms()
{
  $user_code = $_POST['user_code'] != null ? $_POST['user_code'] : 1;
  $date = $_POST['date'] != null ? $_POST['date'] : date("Y-m-d");
  global $con;
  $data = ["date" => $date, "user_code" => $user_code];
  $sql = "SELECT symptom_list.* ,user_symptom_dairy.*
  FROM symptom_list 
  LEFT JOIN user_symptom_dairy 
    ON symptom_list.code=user_symptom_dairy.symptom_code 
    AND user_symptom_dairy.date=:date 
    AND user_symptom_dairy.user_code=:user_code 
  WHERE symptom_list.isShow=1 
  ORDER BY symptom_list.s_category,symptom_list.ordering";
  $stmt = $con->prepare($sql);
  $stmt->execute($data);
  while ($row = $stmt->fetch()) {
    $obj['date'] = $row['date'];
    $obj['code'] = $row['code'];
    $obj['mode'] = $row['mode'];
    $obj['remarks'] = $row['remarks'];
    $obj['suffix'] = $row['suffix'];
    $obj['ordering'] = $row['ordering'];
    $obj['s_title_zh'] = $row['s_title_zh'];
    $obj['s_title_en'] = $row['s_title_en'];
    $obj['s_category'] = $row['s_category'];
    $obj['s_icon'] = $row['s_icon'];
    $obj['s_icon_active'] = $row['s_icon_active'];
    $obj['s_category'] = $row['s_category'];
    $obj['isShow'] = $row['isShow'];
    $obj['scat_title_zh'] = $row['scat_title_zh'];
    $obj['scat_title_en'] = $row['scat_title_en'];
    $symptoms_arr[] = $obj;
  }
  $otherNoteSQL = "SELECT remarks FROM `user_symptom_dairy` 
  WHERE date=:date and symptom_code IS NULL and `user_code`=:user_code";
  $stmt = $con->prepare($otherNoteSQL);
  $stmt->execute($data);
  while ($row = $stmt->fetch()) {
    $noteObj['mode'] = 'Note';
    $noteObj['remarks'] = $row['remarks'];
    $symptoms_arr[] = $noteObj;
  }
  return $symptoms_arr;
}

// function getSymptom(){
//   global $con; 


// }


echo json_encode(getAllSymptoms(), JSON_UNESCAPED_UNICODE);
