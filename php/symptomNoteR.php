<?php

include("Connection/db.php");

if ($_POST['user_code'] != null) {
    $data = ['user_code' => $_POST['user_code'], 'category' => 'Notes'];
    $sql = "SELECT * FROM `user_symptom_dairy`
    LEFT JOIN `symptom_list`
ON `user_symptom_dairy`.`symptom_code`=`symptom_list`.`code`
    where `user_symptom_dairy`.`category`=:category
    and `user_code`=:user_code
    and `user_symptom_dairy`.`remarks` IS NOT NULL
    ORDER BY `user_symptom_dairy`.`date`";
    
    $stmt = $con->prepare($sql);
    $stmt->execute($data);
    while ($row = $stmt->fetch()) {
        $obj['date'] = $row['date'];
        $obj['suffix'] = $row['suffix'];
        $obj['s_title_zh'] = $row['s_title_zh'];
        $obj['s_title_en'] = $row['s_title_en'];
        $obj['remarks'] = $row['remarks'];
        $symptoms_arr[] = $obj;
    }
    echo json_encode($symptoms_arr, JSON_UNESCAPED_UNICODE);
} else {
    echo "{\"status\":\"failed\"}";
}
