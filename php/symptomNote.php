<?php

include("Connection/db.php");
$date = $_POST['date'] == null ? date("Y-m-d") : $_POST['date'];
if (
    $_POST['user_code'] != null
) {
    $deleteionData = ["date" => $_POST['date'], "user_code" => $_POST['user_code']];
    $deleteSQL = "DELETE FROM `user_symptom_dairy` WHERE `user_code`=:user_code"
        . " and `date`=:date and `category`='Notes';";
    $stmt = $con->prepare($deleteSQL);
    $stmt->execute($deleteionData);

    if (
        !($stmt->execute($deleteionData))
    ) {
        die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
    } else {
        if (
            $_POST['scode'] != null && $_POST['svalue'] != null &&
            count($_POST['scode']) == count(($_POST['svalue']))
            && count(($_POST['svalue'])) != 0
        ) {
            foreach ($_POST['scode'] as $key => $value) {
                $insertionData = [
                    "user_code" => $_POST['user_code'],
                    "date" => $date,
                    "code" => $value,
                    "value" => $_POST['svalue'][$key]
                ];
                $insertSql =
                    "INSERT INTO `user_symptom_dairy`(`user_code`, `date`, `category`, `symptom_code`, `remarks`)"
                    . " VALUES (:user_code,:date,'Notes',:code,:value)";
                $stmt = $con->prepare($insertSql);
                if (!($stmt->execute($insertionData))) {
                    die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
                }
            }
        }
        echo "{\"status\":\"success\"}";
    }
} else {
    echo "{\"status\":\"failed\"}";
}
