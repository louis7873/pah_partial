<?php
function white_list(&$value, $allowed, $message)
{
    if ($value === null) {
        return $allowed[0];
    }
    $key = array_search($value, $allowed, true);
    if ($key === false) {
        throw new InvalidArgumentException($message);
    } else {
        return $value;
    }
}
include("Connection/db.php");
$date = date("y-m-d");
if ($_POST['unit'] != null && $_POST['interval'] != null && $_POST['user_code'] != null) {
    $unit = white_list($_POST['unit'], ["YEAR", "MONTH", "WEEK", "DAY", "MINUTE", "SECOND"], "Invalid time unit name");
    $data = ['interval' => $_POST['interval'], 'user_code' => $_POST['user_code']];
    $sql = "SELECT * FROM `user_symptom_dairy` 
        WHERE `symptom_code` IS NOT NULL 
            and `category` = 'Notes' 
            and `date` > DATE_ADD(CURRENT_DATE(), INTERVAL :interval $unit) 
            and `user_code` = :user_code
        ORDER BY date asc,`symptom_code` asc";
    $stmt = $con->prepare($sql);
    $stmt->execute($data);
    while ($row = $stmt->fetch()) {
        $obj['date'] = $row['date'];
        $obj['symptom_code'] = $row['symptom_code'];
        $obj['remarks'] = $row['remarks'];
        $symptoms_arr[] = $obj;
    }
    echo json_encode($symptoms_arr, JSON_UNESCAPED_UNICODE);
}
