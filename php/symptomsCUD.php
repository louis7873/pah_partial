<?php

include("Connection/db.php");

if ($_POST['user_code'] != null and $_POST['date'] != null) {
    $deleteionData = ["date" => $_POST['date'], "user_code" => $_POST['user_code']];
    $deleteSQL = "DELETE FROM `user_symptom_dairy` WHERE `user_code`=:user_code"
        . " and `date`=:date and `category`='Symptom';";
    $deleteNoteSQL = "DELETE FROM `user_symptom_dairy` WHERE date=:date and symptom_code IS NULL and `user_code`=:user_code";
    $stmtDNote = $con->prepare($deleteNoteSQL);
    if (!($stmtDNote->execute($deleteionData))) {
        die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
    }
    $stmtD = $con->prepare($deleteSQL);
    if (!($stmtD->execute($deleteionData))) {
        die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
    } else {
        if ($_POST['note'] != null && $_POST['note'] != "") {
            $data = ["date" => $_POST['date'], "user_code" => $_POST['user_code'], "remarks" => $_POST['note']];
            $insertNoteSql =
                "INSERT INTO `user_symptom_dairy`(`user_code`, `date`, `category`, `remarks`)
                 VALUES (:user_code,:date,'Notes',:remarks)";
            $stmt = $con->prepare($insertNoteSql);
            if (!($stmt->execute($data))) {
                die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
            }
        }
        if ($_POST['scodes'] != null) {
            foreach ($_POST['scodes'] as $value) {
                $insertionData = [
                    "user_code" => $_POST['user_code'],
                    "date" => $_POST['date'],
                    "value" => $value
                ];
                $insertSql =
                    "INSERT INTO `user_symptom_dairy`(`user_code`, `date`, `category`, `symptom_code`)"
                    . " VALUES (:user_code,:date,'Symptom',:value)";
                $stmt = $con->prepare($insertSql);
                if (!($stmt->execute($insertionData))) {
                    die("{\"status\":\"failed\",\"detail\":'" . $conn->error . "'}");
                }
            }
        }
    }
    echo "{\"status\":\"success\"}";
} else {
    echo "{\"status\":\"failed\"}";
}
